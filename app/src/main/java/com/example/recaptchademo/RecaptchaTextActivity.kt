package com.example.recaptchademo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.ProgressBar

class RecaptchaTextActivity : AppCompatActivity() {
    val recaptcha : ImageView? = null
    val answer : EditText? = null
    val verify : Button? = null
    val reload : Button? = null
    val progress : ProgressBar? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recaptcha_text)

    }
}