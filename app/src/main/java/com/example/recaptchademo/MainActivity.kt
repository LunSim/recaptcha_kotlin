package com.example.recaptchademo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import com.android.volley.toolbox.Volley
import com.google.android.gms.safetynet.SafetyNet
import com.google.android.gms.common.api.CommonStatusCodes
import com.google.android.gms.common.api.ApiException
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.google.android.gms.safetynet.SafetyNetApi
import com.google.android.gms.safetynet.SafetyNetApi.RecaptchaTokenResponse
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import org.json.JSONObject
import java.lang.Exception

//Reference:  https://www.javatpoint.com/using-google-recaptcha-in-android-application
class MainActivity : AppCompatActivity(), View.OnClickListener {

    var TAG = MainActivity::class.java.simpleName
    var btnverifyCaptcha: Button? = null
    var queue: RequestQueue? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        queue = Volley.newRequestQueue(applicationContext)

        btnverifyCaptcha = findViewById(R.id.button)
        btnverifyCaptcha?.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.button -> {
                SafetyNet.getClient(this).verifyWithRecaptcha(Config.SITE_KEY)
                    .addOnSuccessListener(this) { response ->
                        if (response.tokenResult.isNotEmpty()) {
                            handleSiteVerify(response.tokenResult)
                        }
                    }
                    .addOnFailureListener(this) { e ->
                        if (e is ApiException) {
                            var apiException = e
                            Log.d(
                                TAG,
                                "Message: ${CommonStatusCodes.getStatusCodeString(apiException.statusCode)} "
                            )
                        } else {
                            Log.d(TAG, "Unknown type of error: " + e.message)
                        }
                    }

            }
        }
    }



    private fun handleSiteVerify(tokenResult: String?) {
        //it is google recaptcha siteverify server
        //you can place your server url
        val url = "https://www.google.com/recaptcha/api/siteverify"
        var request = object : StringRequest(
            Method.POST,
            url,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                try {
                    if (jsonObject.getBoolean("success")){
                        //code logic when captcha returns true
                    Toast.makeText(applicationContext,jsonObject.getBoolean("success").toString(),Toast.LENGTH_LONG).show()
                        val intent = Intent(this,RecaptchaTextActivity::class.java)
                        startActivity(intent)
                    }
                    else{
                        Toast.makeText(applicationContext,jsonObject.getBoolean("error-codes").toString(),Toast.LENGTH_LONG).show()
                    }
                }catch (e : Exception){
                    Log.d(TAG, "JSON exception: " + e.message)
                }
            },
            Response.ErrorListener { error ->
                Log.d(TAG, "Error message: " + error.message)
            }
        ) {
            override fun getParams(): MutableMap<String, String>  {
                var params = HashMap<String, String>()
                params["secret"]  = Config.SECRET_KEY
                params["response"] = tokenResult!!
                return params
            }
        }
        request.retryPolicy =
            object : DefaultRetryPolicy
                (
                50000,
                DEFAULT_MAX_RETRIES,
                DEFAULT_BACKOFF_MULT)
            {}
        queue?.add(request)

    }
}